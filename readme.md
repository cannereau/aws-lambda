# AWS Lambda

This is a Terraform module to build :
- Lambda function

## Input parameters :
- **name** : Function's name
- **package** : Zip file path
- **role** : Fully qualified name (ARN) of Lambda IAM role
- **handler** : Function handler
- **description** : Description (default value : empty string)
- **timeout** : Function timeout in seconds (default value : 30)
- **memory** : Lambda memory size in MB (default value : 128)
- **runtime** : Lambda runtime version (default value : python3.9)
- **architecture** : Lambda instruction set architecture (default value : arm64)
- **storage** : Lambda ephemeral storage size in MB (default value : 512)
- **publish** : Lambda new version to publish (default value : false)
- **retention** : Lambda execution logs retention in days (default value : 30)
- **tags** : Application's tags
- **layers** : Lambda layers (optional : if not present, no layer will be configured)
- **env** : Environment Variables

## Output values :
- **arn** : Function's ARN
- **name** : Function's name
- **dev** : Qualifier of Lambda DEV version
- **prod** : Qualifier of Lambda PROD version
