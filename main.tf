# retrieve lambda error notifyer
data "aws_lambda_function" "error" {
  function_name    = "lz-errors-notif"
}

# define lambda function
resource "aws_lambda_function" "function" {
  function_name    = var.name
  filename         = var.package
  source_code_hash = filebase64sha256(var.package)
  role             = var.role
  handler          = var.handler
  description      = var.description
  timeout          = var.timeout
  memory_size      = var.memory
  runtime          = var.runtime
  architectures    = var.architecture
  publish          = var.publish
  tags             = var.tags
  layers           = var.layers
  ephemeral_storage {
    size = var.storage
  }
  dynamic "environment" {
    for_each = local.environment_map
    content {
      variables = environment.value
    }
  }
}

# define lambda log group
resource "aws_cloudwatch_log_group" "log" {
  name              = "/aws/lambda/${aws_lambda_function.function.function_name}"
  tags              = var.tags
  retention_in_days = var.retention
}

# define subscription filter
resource "aws_cloudwatch_log_subscription_filter" "filter" {
  name            = "lz-errors-lambda"
  log_group_name  = aws_cloudwatch_log_group.log.name
  filter_pattern  = "\"[ERROR]\""
  destination_arn = data.aws_lambda_function.error.arn
}

# define aliases
resource "aws_lambda_alias" "dev" {
  name             = "dev"
  function_name    = aws_lambda_function.function.arn
  function_version = "$LATEST"
}
resource "aws_lambda_alias" "prod" {
  name             = "prod"
  function_name    = aws_lambda_function.function.arn
  function_version = aws_lambda_function.function.version
}
