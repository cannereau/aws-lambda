variable "name" {
  type        = string
  description = "Function's name"
}

variable "package" {
  type        = string
  description = "Zip file path"
}

variable "role" {
  type        = string
  description = "Fully qualified name (ARN) of Lambda IAM role"
}

variable "handler" {
  type        = string
  description = "Function handler"
}

variable "description" {
  type        = string
  default     = ""
  description = "Description"
}

variable "timeout" {
  type        = string
  default     = "30"
  description = "Function timeout in seconds"
}

variable "memory" {
  type        = string
  default     = "128"
  description = "Lambda memory size in MB"
}

variable "runtime" {
  type        = string
  default     = "python3.11"
  description = "Lambda runtime version"
}

variable "architecture" {
  type        = list(string)
  default     = ["arm64"]
  description = "Lambda instruction set architecture"
}

variable "storage" {
  type        = string
  default     = "512"
  description = "Lambda ephemeral storage"
}

variable "publish" {
  type        = bool
  default     = false
  description = "Lambda new version to publish"
}

variable "retention" {
  type        = number
  default     = 30
  description = "Lambda logs retention in days"
}

variable "tags" {
  type        = map(string)
  description = "Application's tags"
}

variable "layers" {
  type        = list(string)
  default     = []
  description = "Lambda layers"
}

variable "env" {
  type        = map
  default     = null
  description = "Environment Variables"
}

locals {
  environment_map = var.env == null ? [] : [var.env]
}
