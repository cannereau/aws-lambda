output "arn" {
  value       = aws_lambda_function.function.arn
  description = "Function's ARN"
}

output "name" {
  value       = aws_lambda_function.function.function_name
  description = "Function's name"
}

output "dev" {
  value       = aws_lambda_alias.dev.name
  description = "Qualifier of Lambda DEV version"
}

output "prod" {
  value       = aws_lambda_alias.prod.name
  description = "Qualifier of Lambda PROD version"
}
